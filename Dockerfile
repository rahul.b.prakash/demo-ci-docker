FROM python:3.6

RUN apt-get update
WORKDIR /setup/
COPY ./requirements.txt /setup/
RUN pip install -r requirements.txt
WORKDIR /src/
COPY ./main.py /src/
CMD python main.py